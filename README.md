# Service Layer

Как только произведется деплой в pipeline, необходимо добавить публичный ключ flux в GitLab. Предварительно проводим авторизацию в кластере командой, предлагаемой в GCP.

`gcloud container clusters get-credentials infra --zone ***** --project ********`

`fluxctl identity --k8s-fwd-ns flux`


